'''
IPv5

1. zadatak

Napišite program koji se izvodi u tri procesa i koristi neblokirajuću komunikaciju:

    proces ranga 1 računa zbroj i produkt parnih prirodnih brojeva manjih ili jednakih 10 i rezultat šalje procesu ranga 0 kao listu koja sadrži dva elementa tipa int ili numpy polje od dva elementa tipa numpy.int32,
    proces ranga 2 računa zbroj i produkt prirodnih brojeva manjih ili jednakih 20 i rezultat šalje procesu ranga 0 kao listu koja sadrži dva elementa tipa int ili kao numpy polje od dva elementa tipa numpy.int64,
    proces ranga 0 prima rezultate i ispisuje ih na ekran.

'''

import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
    print("Potrebna su barem tri procesa za izvođenje")
    exit()

if rank == 0:
    recvmsg1 = np.empty(2, dtype=np.int32)
    recvmsg2 = np.empty(2, dtype=np.int64)

    request1 = comm.Irecv(recvmsg1, source=1)
    request2 = comm.Irecv(recvmsg2, source=2)

    print("Proces ranga 1 poslao je ", recvmsg1)
    print("Proces ranga 2 poslao je ", recvmsg2)
    request1.Wait()
    request2.Wait()
elif rank == 1:
    suma = 0
    produkt = 1
    for i in range(1, 11):
        if i%2 == 0:
            suma= suma + i
            produkt = produkt *i
    #sendmsg = np.array([suma, produkt], dtype=np.int32)                                                                                     
    sendmsg = np.empty(2, dtype=np.int32)
    sendmsg[0]=suma
    sendmsg[1]=produkt
    request1 = comm.Isend(sendmsg, dest=0)
    request1.Wait()
elif rank == 2:
    suma = 0
    produkt = 1
    for i in range (1, 21):
        suma = suma +i
        produkt = produkt *i
    #sendmsg = np.array([suma, produkt], dtype=np.int64)                                                                                     
    sendmsg = np.empty(2, dtype=np.int64)
    sendmsg[0]=suma
    sendmsg[1]=produkt
    request2 = comm.Isend(sendmsg, dest=0)
    request2.Wait()
    MPI.Request.Waitall([request1, request2])

else:
    exit()
