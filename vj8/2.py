import numpy as np

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = 3 * np.ones(10)
    b = 4 * np.ones(10)
    skalarni_produkt = np.empty(1)
    minimum = np.empty(1)
    maksimum = np.empty(1)

    # NOVO
    rezultati = np.empty(10)
else:
    a = None
    b = None
    skalarni_produkt = None

    # NOVO
    minimum = None
    maksimum = None
    rezultati = None

element_a = np.empty(1)
element_b = np.empty(1)

comm.Scatter(a, element_a, root=0)
comm.Scatter(b, element_b, root=0)

rezultat = element_a * element_b

# NOVO
comm.Gather(rezultat, rezultati, root=0)

comm.Reduce(rezultat, skalarni_produkt, op=MPI.SUM, root=0)
comm.Reduce(rezultat, minimum, op=MPI.MIN, root=0)
comm.Reduce(rezultat, maksimum, op=MPI.MAX, root=0)

if rank == 0:
    # c = c.reshape((3,3))
    dotProvjera = np.dot(a,b)
    print("Skalarni produkt iznosi:\n", skalarni_produkt)
    print("\nProvjera skalara:\n", skalarni_produkt == dotProvjera)

    # NOVO
    print("\nProvjera minimuma:\n", minimum == rezultati.min())
    print("\nProvjera maksimuma:\n", maksimum == rezultati.max())
