'''
Zadatak: 

zamijenite kod u nastavku ekvivalentnim kodom koji koristi comm.Gather;
iskoristite vektor dijelovi_rjesenja
'''

import numpy as np

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = 3 * np.ones(10)
    b = 4 * np.ones(10)
    skalarni_produkt = np.empty(1)
    dijelovi_rjesenja = np.empty(10)
else:
    a = None
    b = None
    skalarni_produkt = None
    dijelovi_rjesenja = None

element_a = np.empty(1)
element_b = np.empty(1)

comm.Scatter(a, element_a, root=0)
comm.Scatter(b, element_b, root=0)

rezultat = element_a * element_b

# zamjena ovdje ...
#comm.Reduce(rezultat, skalarni_produkt, op=MPI.SUM, root=0)
comm.Gather(rezultat, dijelovi_rjesenja, root=0)

if rank == 0:
    skalarni_produkt = np.sum(dijelovi_rjesenja)
    print("Skalarni produkt iznosi:\n", skalarni_produkt)
    print("\nProvjera:\n", skalarni_produkt == np.dot(a,b))
