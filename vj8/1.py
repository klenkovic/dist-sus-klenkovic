import numpy as np

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = MPI.COMM_WORLD.Get_rank()
size = MPI.COMM_WORLD.Get_size()

if rank == 0:
    a = np.array([[1,2],[3,4],[5,6]], dtype=np.int64)
    b = np.array([[1,2,3],[4,5,6]], dtype=np.int64)
    c = np.empty((3,3), dtype=np.int64)
    # c = np.empty(9), dtype=np.int64)
else:
    a = np.empty((3,2), dtype=np.int64)
    b = np.empty((2,3), dtype=np.int64)
    c = None

comm.Bcast(a, root=0)
comm.Bcast(b, root=0)

'''
for i in range(3):
    for j in range(3):
        c[i][j] = np.dot(a[i],b[:,j])
'''

element_c = np.dot(a[rank // 3], b[:, rank % 3])

comm.Gather(element_c, c, root=0)

if rank == 0:
    # c = c.reshape((3,3))
    print("A =\n",a)
    print("\nB =\n",b)
    print("\nC = A * B =\n",c)
    print("\nProvjera\n", np.dot(a,b)==c)
