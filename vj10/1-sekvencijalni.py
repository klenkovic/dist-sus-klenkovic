import numpy as np
# import random
# import math

n_random_choices = 100000000000
hits = 0
throws = 0

for i in range (0, n_random_choices):
    throws += 1
    x = np.random.random() # x = random.random()
    y = np.random.random() # y = random.random()
    dist = np.math.sqrt(x * x + y * y) # dist = math.sqrt(x * x + y * y)
    if dist <= 1.0:
        hits += 1

pi = 4 * (hits / throws)

error = abs(pi - np.math.pi) # error = abs(pi - math.pi)
print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
