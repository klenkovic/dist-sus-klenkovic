'''

Zadatak
Iskoristite Comm.Split() da bi razdijelili COMM_WORLD u dva dijela.
Prvi dio neka sadrži procese s parnim rangom iz COMM_WORLD u uzlaznom poretku.
Drugi dio neka sadrži procese s neparnim rangom iz COMM_WORLD u silaznom poretku.

'''

from mpi4py import MPI

world_rank = MPI.COMM_WORLD.Get_rank()
world_size = MPI.COMM_WORLD.Get_size()

if world_rank < world_size // 2:
    color = 55
    key = -world_rank
else:
    color = 77
    key = +world_rank

newcomm = MPI.COMM_WORLD.Split(color, key)
# dobivamo dvije podgrupe procesa koje ne komuniciraju međusobno
newcomm.Free()
