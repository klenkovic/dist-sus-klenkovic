'''

Zadatak
Stvorite grupu procesa koja sadrži samo procese iz grupe COMM_WORLD koji imaju parni rang. Iskoristite novu grupu da bi stvorili novi komunkator. (Uputa: iskoristite Group.Incl(), Group.Range_incl() ili Group.Excl().)

NE RADI!!!!!!!!

'''

from mpi4py import MPI

comm = MPI.COMM_WORLD
group = comm.Get_group()
size = comm.Get_size()

neparniRankovi = list(range(1,size,2))

newgroup = group.Excl(neparniRankovi)
newcomm = comm.Create(newgroup)

#group.Free()

newgroupIncludePristup = group.Incl(list(range(0,size,2)))
newcommIncludePristup = comm.Create(newgroupIncludePristup)

if comm.Get_rank() == 0:
    assert newcomm.Get_size() == newcommIncludePristup.Get_size()

'''
group.Free()
newgroup.Free()

if newcomm:
    newcomm.Free()
'''
