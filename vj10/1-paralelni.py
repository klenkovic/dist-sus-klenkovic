'''
Zadatak
Modificirajte primjer dan za Monte Carlo simulaciju da dodate kod koji mjeri vrijeme izvođenja za svaki od procesa.
Usporedite vrijeme izvođenja simulacije za 2, 3, 4 procesa kad svaki proces izvodi 10^4^, 10^5^, 10^6^ iteracija. Opišite svoje zaključke.


Zadatak
Usporedite vrijeme izvođenja programa i točnost aproksimacije pi korištenjem:
sumiranja elemenata niza,
Monte Carlo simulacije,
za 2, 3, 4 procesa kad svaki proces izvodi 10^4^, 10^5^, 10^6^ iteracija. Opišite svoje zaključke.

'''

import numpy as np
from sys import argv
from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

try:
    n_random_choices = int(argv[1])
except IndexError:
    n_random_choices = 100000

hits = 0
throws = 0

print("Svaki proces ce napraviti", n_random_choices // size, "iteracija; total", size * (n_random_choices // size), "iteracija")

timeBegin = time.time()

# dodan "// size" - manje experimenata
for i in range (0, n_random_choices // size):
    throws += 1
    x = np.random.random()
    y = np.random.random()
    dist = np.math.sqrt(x * x + y * y)
    if dist <= 1.0:
        hits += 1

pi_part = np.empty(1)
pi_part[0] = 4 * (hits / throws)

if rank == 0:
    pi_reduced = np.empty(1)
else:
    pi_reduced = None

comm.Reduce(pi_part, pi_reduced, op=MPI.SUM, root=0)

timeEnd = time.time()

print("Proces ranga", rank, "izvrsio se u", timeEnd-timeBegin, "sekundi")

if rank == 0:
    pi = pi_reduced[0] / size
    error = abs(pi - np.math.pi)
    print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))
    print
