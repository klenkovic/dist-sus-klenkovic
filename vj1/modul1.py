def zbrajanje(x, y):
    """
    Funkcija vrši zbrajanje 2 broja.

    Argumenti:
    x -- prvi broj
    y -- drugi broj

    Vraća:
    Zbroj broja x i y.
    """
    return x + y

def mnozenje(x, y):
    """                                                                                                                                     
    Funkcija vrši množenje 2 broja.                                                                                                                                            
    Argumenti:                                                                                                                               
    x -- prvi broj                                                                                                                          
    y -- drugi broj                                                                                                                         
                                                                                                                                            
    Vraća:                                                                                                                                  
    Zbroj umnožak x i y.                                                                                                                    
    """
    return x * y

def potenciranje(x, y):
    """
    Funkcija potencira broj x brojem y.

    Argumenti:
    x -- baza
    y -- potencija

    Vraća:
    Potencija broja x brojem y.
    """
    return x ** y

def dijeljenje(x, y):
    """
    Funkcija dijeli 2 broja

    Argumenti:
    x -- dijeljenik
    y -- dijelitelj

    Vraća:
    Količnik brojeva x i y.
    """
    return x/y
