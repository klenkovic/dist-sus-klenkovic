import modul1 as m1

assert m1.zbrajanje(2, 3) == 5
assert m1.zbrajanje(23, 4) == 27
assert m1.zbrajanje(24, 81) == 105

print("Testovi f-je zbrajanje su uspješno izvršeni.")

assert m1.mnozenje(2,2) == 4
assert m1.mnozenje(16,4) == 64
assert m1.potenciranje(2,3) == 8
assert m1.potenciranje(24,3) == 13824

print("Testovi f-ja množenja i potenciranja su uspješno izvršeni.")


assert round(m1.dijeljenje(4, 5), 1) == 0.8
assert m1.dijeljenje(20, 5) == 4
assert round(m1.dijeljenje(2, 3), 4) == 0.6667

# Dodajte još jedan test sa 2 broja koji nisu cijelobrojni.
# Zaokružite rješenje na 5 decimala.
assert round(m1.dijeljenje(123.321, 321.123), 5) == 0.38403

print("Testovi f-ja dijeljenja su uspješno izvršeni.")


# Napišite po 2 testa za f-ju množenja i potenciranje.
# Iskoristite alat Calculator da bi lakše izračunali vrijednosti.

# help(m1.zbrajanje)
