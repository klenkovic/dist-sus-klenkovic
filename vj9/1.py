# Proširite navedeni kod da umjesto produkta matrice i vektora računa produkt dvaju matrica.

# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# x = np.array([10, 11, 12], dtype=np.float32)

if rank == 0:
    A = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [11, 22, 33]], dtype=np.float32)
    # x = np.array([10, 11, 12], dtype=np.float32)
    B = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]], dtype=np.float32)
    C = np.empty((4,4), dtype=np.float32)
else:
    A = None
    B = np.empty((3,4), dtype=np.float32)
    # x = np.empty(3, dtype=np.float32)
    C = None

redak_A = np.empty(3, dtype=np.float32)
# redak_C = np.empty(4, dtype=np.float32)
comm.Scatter(A, redak_A, root=0)
comm.Bcast(B, root=0)

redak_C = np.dot(redak_A, B)
comm.Gather(redak_C, C, root=0)

if rank == 0:
    print("Produkt matrice\n", A)
    print("i matrice\n", B)
    print("iznosi\n", C)
    print(np.dot(A,B)==C)
    
