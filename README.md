# Distribuirani sustavi #

**Kristijan Lenković**

*[Novi HgWiki](http://inf2.uniri.hr/reStructuredHgWiki)*: http://inf2.uniri.hr/reStructuredHgWiki

*[Enroll link](https://canvas.instructure.com/enroll/NMMKDJ)*: https://canvas.instructure.com/enroll/NMMKDJ

Spajanje na Akston:
ssh -X -p 2223 student17@inf2.uniri.hr

cssh app spajanje na ostala računala u mreži:
cssh -u student17 galt.local rearden.local danconia.local danneskjold.local

## VJEŽBE ##

1. Uvod u Python 3
2. Uvod u NumPy
3. Osnovni pojmovi paralelnog, distribuiranog i heterogenog računarstva, blokirajuća komunikacija
4. Neblokirajuća komunikacija, primjene komunikacije točka-do-točke
5. Primjena komunikacije točka-do-točke (nastavak), kolektivna komunikacija (Barrier, Bcast)
6. Nastavak kolektivne komunikacije (Scatter, Gather, Allgather, primjena)

1. Kolokvij

7. Git
8. Množenje matrica s Bcast i Gather,  korištenje Reduce funkcije sa SUM operacijom.
9. ?
10. Monte Carlo simulacije, komunikatori i grupe procesa
11. Rad na više računala (preko Akstona)