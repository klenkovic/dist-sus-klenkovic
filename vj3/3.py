'''

Zadatak

Napišite program koji se izvodi u tri procesa i koristi blokirajuću komunikaciju:

    proces ranga 1 računa zbroj i produkt parnih prirodnih brojeva manjih ili jednakih 10 i rezultat šalje procesu ranga 0 kao listu koja sadrži dva elementa tipa int ili numpy polje od dva elementa tipa numpy.int32,
    proces ranga 2 računa zbroj i produkt prirodnih brojeva manjih ili jednakih 20 i rezultat šalje procesu ranga 0 kao listu koja sadrži dva elementa tipa int ili kao numpy polje od dva elementa tipa numpy.int64,
    proces ranga 0 prima rezultate i ispisuje ih na ekran.


'''

# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
    print("Potrebna su barem tri procesa za izvođenje")
    exit()

if rank == 0:

    recvmsg1 = np.empty(2, dtype=np.int32)
    recvmsg2 = np.empty(2, dtype=np.int64)
    comm.Recv(recvmsg1, source=1)
    comm.Recv(recvmsg2, source=2)
    
    print('Rezultati iz procesa identifikatora 1 su:', recvmsg1)
    print('Rezultati iz procesa identifikatora 2 su:', recvmsg2)

elif rank == 1:

    suma = 0
    produkt = 1

    for i in range(1,11):
        if i % 2 == 0:
            suma =+ i
            produkt *= i

    sendmsg = np.array([suma, produkt], dtype=np.int32)
    comm.Send(sendmsg, dest=0)

elif rank == 2:

    suma = 0
    produkt = 1

    for i in range(1,21):
        if i % 2 == 0:
            suma =+ i
            produkt *= i
    
    sendmsg = np.array([suma, produkt], dtype=np.int64)
    comm.Send(sendmsg, dest=0)


else:
    exit()
