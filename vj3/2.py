'''

Zadatak

Modficirajte program tako da poruke razmjenjuju tri procesa, i to tako da proces 0 šalje podatak tipa po vašoj želji i vrijednosti po vašoj želji procesu 1, proces 1 šalje isti podatak procesu 2, a proces 2 podatak šalje procesu 0.

'''

# varijanta s Python objektima
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
    print("Potrebna su barem tri procesa za izvođenje")
    exit()

if rank == 0:
    sendmsg = "Pozdrav!"
    comm.send(sendmsg, dest=1)
    recvmsg = comm.recv(source=2)
    print("Primljena poruka:", recvmsg)
elif rank == 1:
    recvmsg = comm.recv(source=0)
    comm.send(recvmsg, dest=2)
elif rank == 2:
    recvmsg = comm.recv(source=1)
    comm.send(recvmsg, dest=0)
else:
    print("Proces ranga", rank, "ne razmjenjuje poruke")
    exit()
