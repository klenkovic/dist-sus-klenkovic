'''
Zadatak

    Pokrenite kod tako da se izvodi u 8 procesa.
    Učinite da procesi s parnim identifikatorom ispisuju i Ja sam proces sa parnim identifikatorom, a procesi s neparnim identifikatorom ispisuju Ja sam proces s neparnim identifikatorom.
    Dodajte da procesi s neparnim identifikatorom pored toga ispisuju i Kvadrat mog identifikatora iznosi te kvadrat svog identifikatora.

'''

# $ mpirun -np 2 python3 1.py

from mpi4py import MPI

name = MPI.Get_processor_name()
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

print("Pozdrav sa domaćina", name, "od procesa ranga", rank, "od ukupno", size, "procesa")

if rank == 0:
    vendor = MPI.get_vendor()
    print("Podaci o implementaciji MPI-a koja se koristi:", vendor[0], vendor[1])
if rank % 2 == 0:
    print("Ja sam proces s parnim identifikatorom:", rank)
if rank % 2 != 0:
    print("Ja sam proces s NEparnim identifikatorom:", rank)
    print("Kvadrat mog identifikatora iznosi", rank**2)
