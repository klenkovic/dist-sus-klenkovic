'''
Zadatak

    Promijenite kod da su vektori veličine 6 elemenata umjesto 4, i izvedite kod u 6 procesa.
    Dodajte još jedan vektor veličine 6 elemenata i izračunajte zbroj tri vektora umjesto zbroja dva vektora.

'''
#$ mpirun -np 6 primjena1.py

# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size == 6

if rank == 0:
    a = np.array([1, 2, 3, 9, 3, 4], dtype=np.int32)
    b = np.array([4, 5, 6, 1, 5, 0], dtype=np.int32)
    c = np.array([5, 3, 1, 0, 2, 6], dtype=np.int32)
    zbroj_vektor = np.empty(6, dtype=np.int32)
else:
    a = None
    b = None
    c = None
    zbroj_vektor = None

element_a = np.empty(1, dtype=np.int32)
comm.Scatter(a, element_a, root=0)
element_b = np.empty(1, dtype=np.int32)
comm.Scatter(b, element_b, root=0)
element_c = np.empty(1, dtype=np.int32)
comm.Scatter(c, element_c, root=0)


element_zbroj = element_a + element_b + element_c
print("Proces ranga", rank, "izračunao je zbroj", element_zbroj)

comm.Gather(element_zbroj, zbroj_vektor, root=0)
if rank == 0:
    print("Zbroj vektora a, b i c iznosi", zbroj_vektor)
