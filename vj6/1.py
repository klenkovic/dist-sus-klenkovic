'''

Zadatak

Modificirajte kod tako da se izvodi u 5 procesa, a scatter se vrši nad matricom proizvoljnih vrijednosti koja ima 5 redaka i 4 stupca zapisanom u obliku liste, odnosno numpy polja.

Učinite da svaki od procesa posljednji element u primljenom retku matrice postavlja na vrijednost 0, a zatim ispisuje taj redak na ekran.


'''

#$ mpirun -np 5 python3 1.py

# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size == 5

if rank == 0:
    sendmsg = np.empty((5,4), dtype=np.int32)
    a = np.arange(4, dtype=np.int32)
    for i in range(0,5):
        sendmsg[i]=a
else:
    sendmsg = None

#print(sendmsg)

recvmsg = np.empty(4, dtype=np.int32)
comm.Scatter(sendmsg, recvmsg, root=0)
print("Proces ranga", rank, "ima vrijednost poruke za slanje", sendmsg, "primljena poruka", recvmsg)

recvmsg[-1]=0
print("Nakon postavljanja na 0:", recvmsg)
