'''
Zadatak

Promijenite program da se izvodi u 5 procesa, i to tako da svaki od procesa inicijalizira listu, odnosno polje, duljine 4 u kojem je prvi element njegov rang, a ostali elementi su slučajne vrijednosti u rasponu od 0.0 do 1.0.

Napravite operaciju gather sa korijenskim procesom ranga 3, te operaciju allgather. Svi procesi neka oba rezultata ispišu na ekran.

'''

#$ mpirun -np 5 python3 2.py

# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size >= 4

sendmsg = np.random.rand(4)
sendmsg[0] = rank

print("Proces ranga", rank, "ima vrijednost poruke za slanje", sendmsg)

if rank == 3:
    recvmsg1 = np.empty((size,4), dtype=np.float64)
else:
    recvmsg1 = None

comm.Gather(sendmsg, recvmsg1, root=3)

recvmsg2 = np.empty((size,4), dtype=np.float64)

comm.Allgather(sendmsg, recvmsg2)

print("Proces ranga", rank, "ima vrijednost prve primljene poruke", recvmsg1, "i druge primljene poruke", recvmsg2)
