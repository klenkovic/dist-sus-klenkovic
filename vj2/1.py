import numpy as np

'''
Zadatak

    Stvorite polje s vrijednostima

    9 13 5
    1 11 7
    3 7 2
    6 0 7

    Saznajte mu oblik, duljinu i tip elemenata i veličinu elementa u bajtovima.

    Stvorite polje s istim vrijednostima, ali tako da su elementi tipa float.
'''

a = np.array([[9,13,5],[1,11,7],[3,7,2],[6,0,7]])
b = np.array([[9,13,5],[1,11,7],[3,7,2],[6,0,7]], dtype=float)

print("Oblik a:", a.shape)
print("Veličina od a:", a.size)
print("Tip polja od a:", a.dtype)
print("Veličina u bajtima od a:", a.itemsize)

print(a)
print(b)

print("=========================================")

'''
Zadatak

    Stvorite polje nula oblika (5, 5) u kojem su elementi tipa numpy.float32.
    Stvorite polje jedinica oblika (1000, 1000). Pokušajte ga ispisati naredbom print. Što se dogodi?
'''

c = np.zeros((5,5), dtype=np.float32)
d = np.ones((1000,1000))

print(c)
# Prikazivanje cijelog polja
# np.set_printoptions(threshold=np.nan)
print(d)

print("=========================================")

'''

Zadatak

    Stvorite dva dvodimenzionalna polja a i b oblika (3, 3) s proizvoljnim vrijednostima,
    i to tako da prvo ima elemente tipa numpy.float32, a drugo elemente tipa numpy.float64.
    Izračunajte 2 * a + b, cos(a), sqrt(b). Uočite kojeg su tipa polja koja dobivate kao rezultate.
    Množenje matrica izvodite funkcijom numpy.dot(); proučite njenu dokumentaciju i izvedite ju na svojim poljima.

'''

