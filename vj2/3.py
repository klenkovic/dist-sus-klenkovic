import numpy as np

'''
Zadatak

    Stvorite dvije datoteke, nazovite ih matrica_a.txt i matrica_b.txt. Matrica u prvoj datoteci neka bude oblika (3, 5), a u drugoj datotec    i oblika (5, 4).
    Izvršite čitanje podataka, a zatim izračunajte produkt dvaju matrica. Možete li izračunati oba produkta ili samo jedan? Objasnite zašto.
'''

aTxt = open("matrica_a.txt")
bTxt = open("matrica_b.txt")
a = np.loadtxt(aTxt)
b = np.loadtxt(bTxt)
aTxt.close()
bTxt.close()

print("Produkt matrica a i b:")
print(np.dot(a,b))
