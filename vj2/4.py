import numpy as np

a = np.array([1,54,3,1,67,54,3,2,54,7,3])
print("a =", a)
print("a[3] =", a[3])
print("a[2:6] =", a[2:6])
print("a[:8:2] = 1337")
a[:8:2] = 1337
print("a =", a)
print("a[[1,3,4]] = 0")
a[[1,3,4]] = 0
print("a =", a)
print("a[::-2] =", a[::-2])
print("a[0] * a[2] -1 =", a[0] * a[2] -1)
print("a[[0,0,2]] = [1,2,3]")
a[[0,0,2]] = [1,2,3]
print("a =", a)
