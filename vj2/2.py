import numpy as np

''' 
Zadatak
    Stvorite dva dvodimenzionalna polja a i b oblika (3, 3) s proizvoljnim vrijednostim,
    i to tako da prvo ima elemente tipa numpy.float32, a drugo elemente tipa numpy.float64.
    Izračunajte 2 * a + b, cos(a), sqrt(b). Uočite kojeg su tipa polja koja dobivate kao rezultate.
    Množenje matrica izvodite funkcijom numpy.dot(); proučite njenu dokumentaciju i izvedite ju na svojim poljima.
'''

a = np.array([[1,2,3],[4,5,6],[7,8,9]], dtype=np.float32)
b = np.array([[9,8,7],[6,5,4],[3,2,1]], dtype=np.float64)

print("2 * a + b =\n", 2 * a + b)
print("cos(a) =\n", np.cos(a))
print("sqrt(b) =\n", np.sqrt(b))

print("Množenje matrica A * B:")
print(np.dot(a,b))


print("====================================================")

'''
Zadatak

Iskoristite dva dvodimenzionalna polja iz prethodnog zadatka da izračunajte 2 * a + b, ali tako da pretvorite drugo u polje koje ima elementa tipa numpy.float32.
'''

b = b.astype(np.float32)

print("Tip od a:", a.dtype)
print("Tip od b:", b.dtype)

print("2 * a + b =\n", 2 * a + b)

print("====================================================")


'''
Zadatak

    Stvorite polje u kojem su sve vrijednosti jednake 9.45 tipa float64 i pretvorite ga u polje tipa float32
    i rezultat spremite u novo polje. Uočavate li gubitak preciznosti?
    Pretvorite dobiveno polje tipa float32 u polje tipa float64. Je li rezultat jednak početnom polju?
    Iskoristite round da na rezultirajućem polju tipa float64 dobijete iste vrijednosti kao na početnom.

Napomena: rezultat ovog zadatka uvelike ovisi o računalu na kojem radite.
'''

c64 = np.ones((3,3), dtype=np.float64) * 9.45
c32 = c64.astype(np.float32)
c64novi = c32.astype(np.float64)
c64round = np.round(c64novi)

print(c64)
print(c32)
print(c64novi)
print(c64round)
