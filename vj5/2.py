'''
Broadcast

Zadatak

 - Prilagodite kod primjera tako da se broadcastom šalje lista s 4 podliste s po 4 elementa, odnosno polje oblika (4, 4), u oba slučaja s proizvoljnim vrijednostima.
 - Učinite da zatim proces ranga 2 promijeni vrijednost elementa na poziciji (1, 1) tako da je uveća za 5, i napravite broadcast s njega svim ostalima.

 - Dodajte treći broadcast s procesa ranga 1, neka se radi o 2D polju s vrijednostima tipa float32 formata (5,15);

np.array([range(x + 15) for x in range(5)])

Svaki od procesa po primitku polja ispisuje na ekran vrijednost na koordinati (rang,rang).

Na svim procesima ispišite primljene podatke na ekran da provjerite je li operacija bila uspješna.

'''

#$ mpirun -np 3 python3 2.py

# varijanta s NumPy poljima
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size >= 3

if rank == 0:
    msg = np.array([[1.5, 2.5, 3.5, 4.5], [2.5, 3.5, 4.5, 5.5], [3.5, 4.5, 5.5, 6.5], [4.5, 5.5, 6.5, 7.5]])
else:
    msg = np.empty((4,4))

print("Prije broadcasta proces ranga", rank, "ima vrijednost varijable", msg)
comm.Bcast(msg, root=0)
print("Nakon broadcasta proces ranga", rank, "ima vrijednost varijable", msg)

if rank == 2:
    msg[0][0] += 5 

comm.Bcast(msg, root=2)
print("Nakon broadcasta (2) proces ranga", rank, "ima vrijednost varijable", msg)

if rank == 1:
    msg2 = np.array([list(range(x,x + 15)) for x in range(5)], dtype=np.float32)
else:
    msg2 = np.empty((5,15), dtype=np.float32)

comm.Bcast(msg2, root=1)
print("Nakon broadcasta (1) proces ranga", rank, "ima vrijednost varijable", msg2)
print(msg2[rank, rank])
